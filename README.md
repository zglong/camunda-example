 1. 正确创建SpringBoot项目,确保可以启动
 2. 添加依赖
    - 修改pom.xml添加camunda相关依赖:
        ``` xml
        <dependencyManagement>
            <dependencies>
                <dependency>
                    <groupId>org.camunda.bpm</groupId>
                    <artifactId>camunda-bom</artifactId>
                    <version>${camunda.version}</version>
                    <scope>import</scope>
                    <type>pom</type>
                </dependency>
                <dependency>
                    <groupId>org.springframework</groupId>
                    <artifactId>spring-framework-bom</artifactId>
                    <version>${spring.version}</version>
                    <scope>import</scope>
                    <type>pom</type>
                </dependency>
            </dependencies>
        </dependencyManagement>
        
        
        <dependency>
            <groupId>org.camunda.bpm</groupId>
            <artifactId>camunda-engine</artifactId>
        </dependency>
        <dependency>
            <groupId>org.camunda.bpm</groupId>
            <artifactId>camunda-engine-spring</artifactId>
        </dependency>
        
        ```
    - 添加数据库相关依赖
        ``` xml
        <!-- 数据库相关依赖 -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>${mysql.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
        </dependency>
        ```
    

 3. 配置文件
    - 添加数据源配置
    
    ``` yml
    spring:
      # 数据源配置
      datasource:
        driver-class-name: com.mysql.jdbc.Driver
        url: jdbc:mysql://10.192.1.230:3306/camunda-example?useUnicode=true&useSSL=false&characterEncoding=UTF-8&serverTimezone=UTC
        username: root
        password: 123456l
      jpa:
        hibernate:
          ddl-auto: validate
        show-sql: true
        
    ```
4. 添加配置类,注册bean

    ``` java
    package com.camunda.example.config;
    
    
    import org.camunda.bpm.engine.HistoryService;
    import org.camunda.bpm.engine.ManagementService;
    import org.camunda.bpm.engine.ProcessEngine;
    import org.camunda.bpm.engine.RepositoryService;
    import org.camunda.bpm.engine.RuntimeService;
    import org.camunda.bpm.engine.TaskService;
    import org.camunda.bpm.engine.spring.ProcessEngineFactoryBean;
    import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    import org.springframework.jdbc.datasource.DataSourceTransactionManager;
    import org.springframework.transaction.PlatformTransactionManager;
    
    import javax.sql.DataSource;
    
    
    /**
     * @Classname CamundaContext
     * @Description 注册camunda相关bean
     * @Date 2020/10/21/ 0021 12:04
     * @Created by zglong
     */
    @Configuration
    public class CamundaContextConfig {
    
    
        @Bean
        public PlatformTransactionManager transactionManager(DataSource dataSource) {
            return new DataSourceTransactionManager(dataSource);
        }
    
    
        @Bean
        public SpringProcessEngineConfiguration engineConfiguration(DataSource dataSource, PlatformTransactionManager transactionManager) {
            SpringProcessEngineConfiguration configuration = new SpringProcessEngineConfiguration();
    
            configuration.setProcessEngineName("engine");
            configuration.setDataSource(dataSource);
            configuration.setTransactionManager(transactionManager);
            configuration.setDatabaseSchemaUpdate("true");
            configuration.setJobExecutorActivate(false);
    
            return configuration;
        }
    
    
        @Bean
        public ProcessEngineFactoryBean engineFactory(SpringProcessEngineConfiguration engineConfiguration) {
            ProcessEngineFactoryBean factoryBean = new ProcessEngineFactoryBean();
            factoryBean.setProcessEngineConfiguration(engineConfiguration);
            return factoryBean;
        }
    
    
        @Bean
        public ProcessEngine processEngine(ProcessEngineFactoryBean factoryBean) throws Exception {
            return factoryBean.getObject();
        }
    
    
        @Bean
        public RepositoryService repositoryService(ProcessEngine processEngine) {
            return processEngine.getRepositoryService();
        }
    
    
        @Bean
        public RuntimeService runtimeService(ProcessEngine processEngine) {
            return processEngine.getRuntimeService();
        }
    
    
        @Bean
        public TaskService taskService(ProcessEngine processEngine) {
            return processEngine.getTaskService();
        }
    
    
        @Bean
        public HistoryService historyService(ProcessEngine processEngine) {
            return processEngine.getHistoryService();
        }
    
    
        @Bean
        public ManagementService managementService(ProcessEngine processEngine) {
            return processEngine.getManagementService();
        }
    }
    ```

    
4. 启动项目,查看数据库,发现生成数据表,总共47张
    ![enter description here](https://notebook-1253455604.cos.ap-shanghai.myqcloud.com/zglong/1603265211248.png)
5. 简单测试
    - 在resource/static/process下创建流程图:
    ![enter description here](https://notebook-1253455604.cos.ap-shanghai.myqcloud.com/zglong/1603265322943.png)

    
    - 编写测试代码
        ``` java
        package com.camunda.example;
        
        
        import org.camunda.bpm.engine.RepositoryService;
        import org.camunda.bpm.engine.repository.Deployment;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.SpringBootApplication;
        import org.springframework.util.StringUtils;
        import org.springframework.web.bind.annotation.GetMapping;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;
        
        
        @SpringBootApplication
        @RestController
        @RequestMapping("/example")
        public class CamundaExampleApplication {
            /**
             * logger
             */
            private static final Logger LOGGER = LoggerFactory.getLogger(CamundaExampleApplication.class);
            @Autowired
            private RepositoryService repositoryService;
        
        
            @GetMapping("/deploy")
            public String deploy(String proKey) {
                if (StringUtils.isEmpty(proKey)) {
                    proKey = "camunda-example";
                }
                try {
                    Deployment deploy = repositoryService.createDeployment()
                            .addClasspathResource("static/processes/" + proKey + ".bpmn").deploy();
                    return "部署ID："+deploy.getId();
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.error(e.getMessage());
                    return e.getMessage();
                }
            }
        
        
            public static void main(String[] args) {
                SpringApplication.run(CamundaExampleApplication.class, args);
            }
        
        
        }
        ```
    - 访问`http://10.192.1.230:8080/example/deploy`:
    ![enter description here](https://notebook-1253455604.cos.ap-shanghai.myqcloud.com/zglong/1603265461539.png)
    - 查看数据库的`act_ge_bytearray`:
    ![enter description here](https://notebook-1253455604.cos.ap-shanghai.myqcloud.com/zglong/1603265549878.png)

