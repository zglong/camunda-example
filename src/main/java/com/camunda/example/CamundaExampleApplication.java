package com.camunda.example;


import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.Deployment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
@RequestMapping("/example")
public class CamundaExampleApplication {
    /**
     * logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CamundaExampleApplication.class);
    @Autowired
    private RepositoryService repositoryService;


    @GetMapping("/deploy")
    public String deploy(String proKey) {
        if (StringUtils.isEmpty(proKey)) {
            proKey = "camunda-example";
        }
        try {
            Deployment deploy = repositoryService.createDeployment()
                    .addClasspathResource("static/processes/" + proKey + ".bpmn").deploy();
            return "部署ID："+deploy.getId();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            return e.getMessage();
        }
    }


    public static void main(String[] args) {
        SpringApplication.run(CamundaExampleApplication.class, args);
    }


}
